import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { FcSearch, FcOpenedFolder, FcInfo } from "react-icons/fc";
import { MdCleaningServices } from "react-icons/md";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Form.css";

function Form(props) {
  const [instalacionList, setInstalacionList] = useState(["Select an option"]);
  const [instalacionSelected, setInstalacionSelected] = useState("Select an option");
  const [estadoList, setEstadoList] = useState(["Select an option"]);
  const [estadoSelected, setEstadoSelected] = useState("Select an option");
  const [sistemaSelected, setSistemaSelected] = useState("");
  const [idSelected, setIdSelected] = useState("");
  const [tipologiaList, setTipologiaList] = useState(["Select an option"]);
  const [tipologiaSelected, setTipologiaSelected] = useState("Select an option");
  const [divisaSelected, setDivisaSelected] = useState("Select an option");
  const [finiSelected, setFiniSelected] = useState("");
  const [finiFinishedSelected, setFiniFinishedSelected] = useState("");
  const [empresaSelected, setEmpresaSelected] = useState("");
  const [disabledSearch, setDisabledSearch] = useState(true);

  useEffect(() => {
    props.officeList.map((office, index) => {
      //Populate instalacionList
      if (instalacionSelected !== "Select an option" || estadoSelected!=="Select an option" || tipologiaSelected!=="Select an option" || idSelected!=="" || sistemaSelected!=="" || empresaSelected!=="" || finiSelected!=="" || finiFinishedSelected!==""){
        setDisabledSearch(false);
      } else{
        setDisabledSearch(true);
      }
      if (instalacionList.includes(office.instalacion));
      else {
        setInstalacionList(instalacionList.concat(office.instalacion));
      }
      //Populate estadolist
      if (estadoList.includes(office.estado));
      else {
        setEstadoList(estadoList.concat(office.estado));
      }
      //Populate tipologiaList
      if (tipologiaList.includes(office.tipologiaContable));
      else {
        setTipologiaList(tipologiaList.concat(office.tipologiaContable));
      }
    });
  });

  const handleCleanInstalacion = () => {
    setInstalacionSelected("Select an option");
  };
  const handleCleanSistema = () => {
    setSistemaSelected("");
  };
  const handleCleanId = () => {
    setIdSelected("");
  };
  const handleClearDates = () => {
    setFiniSelected("");
    setFiniFinishedSelected("");
  }
  const handleCleanEstado = () => {
    setEstadoSelected("Select an option");
  };
  // const handleCleanEmpresa = () => {
  //   setEmpresaSelected("");
  // }
  const handleCleanTipologia = () => {
    setTipologiaSelected("Select an option");
  };
  const handleCleanDivisa = () => {
    setDivisaSelected("Select an option");
  };

  const handleCleanAll = () => {
    setInstalacionSelected("Select an option");
    setSistemaSelected("");
    setIdSelected("");
    handleClearDates("");
    setEstadoSelected("Select an option");
    setEmpresaSelected("");
    setTipologiaSelected("Select an option");
    setDivisaSelected("Select an option");
    setDisabledSearch("true");
    

    props.parentCallback(props.officeList);
  };
  const handleFilter = () => {
    var newFilteredOffices = props.officeList;
    if (instalacionSelected !== "Select an option") {
      newFilteredOffices = newFilteredOffices.filter((office) => {
        return office.instalacion === instalacionSelected;
      });
    }
    if (estadoSelected !== "Select an option") {
      newFilteredOffices = newFilteredOffices.filter((office) => {
        return office.estado === estadoSelected;
      });
    }
    if (tipologiaSelected !== "Select an option") {
      newFilteredOffices = newFilteredOffices.filter((office) => {
        return office.tipologiaContable === tipologiaSelected;
      });
    }
    if (idSelected !== "") {
      newFilteredOffices = newFilteredOffices.filter((office) => {
        return office.idProt == idSelected;
    });
  }
  if (sistemaSelected !== ""){
    newFilteredOffices = newFilteredOffices.filter((office) =>{
      return office.sistema.toLocaleLowerCase() == sistemaSelected.toLocaleLowerCase();
    })
  }
  if (empresaSelected !== ""){
    newFilteredOffices = newFilteredOffices.filter((office) =>{
      return office.empresa.toLocaleLowerCase().includes(empresaSelected.toLocaleLowerCase());
    })
  }
  if(finiSelected !== ""){
    newFilteredOffices = newFilteredOffices.filter((office) => {
      return office.fini >= finiSelected;
    });
  }
  if(finiFinishedSelected !== ""){
    newFilteredOffices = newFilteredOffices.filter((office) => {
      return office.fini <= finiFinishedSelected;
    });
  }

  props.parentCallback(newFilteredOffices);
}

  return (
    <div>
      <form>
        <div className="form">
          <p className="generaltext"> General </p>
          <Row>
            <Col md={6}>
              <div className="input_icon">
                <label>
                <div className="title_label ">Instalación:</div>
                  <select
                    type="text"
                    name="name"
                    className="input_normal"
                    value={instalacionSelected}
                    onChange={(e) => setInstalacionSelected(e.target.value)}>
                    {instalacionList.map((instalacion, index) => (
                      <option>{instalacion}</option>
                    ))}
                  </select>
                </label>
                <div className="icon">
                  <MdCleaningServices onClick={handleCleanInstalacion} />
                  <FcInfo />
                </div>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label ">Sistema origen:</div>
                  <input
                    type="text"
                    name="name"
                    className="input_normal"
                    value={sistemaSelected}
                    onChange={(e) => setSistemaSelected(e.target.value)}
                  />
                </label>
                <div className="icon">
                  <MdCleaningServices onClick={handleCleanSistema} />
                </div>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label ">Id Prot:</div>
                  <input
                    type="text"
                    name="name"
                    className="input_normal"
                    value={idSelected}
                    onChange={(e) => setIdSelected(e.target.value)}
                  />
                </label>
                <div className="icon">
                  <MdCleaningServices onClick={handleCleanId} />
                </div>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label ">F. Ini.:</div>
                  <input type="date" className="input_half" value={finiSelected}
                    onChange={(e) => setFiniSelected(e.target.value)} /> 
                  <input type="date" className="input_half" value={finiFinishedSelected}
                    onChange={(e) => setFiniFinishedSelected(e.target.value)}/>
                </label>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label ">Estado:</div>
                  <select
                    type="text"
                    name="name"
                    className="input_normal"
                    value={estadoSelected}
                    onChange={(e) => setEstadoSelected(e.target.value)}>
                    {estadoList.map((estado) => (
                      <option>{estado}</option>
                      ))}
                  </select>
                </label>
                <div className="icon">
                  <MdCleaningServices onClick={handleCleanEstado} />
                  <FcInfo />
                </div>
              </div>
              <label>
              <div className="title_label ">Desc. semántica:</div>
                <input type="text" name="name" className="input_normal" disabled/>
              </label>
            </Col>

            <Col md={6}>
              <div className="input_icon">
                <label>
                <div className="title_label ">Empresa:</div>
                  <input type="text" name="name" className="input_normal" value={empresaSelected}  onChange={(e) => setEmpresaSelected(e.target.value)}/>
                </label>
                <div className="icon">
                  <FcOpenedFolder />
                  <FcSearch />
                </div>
              </div>
              <div className="input_icon">
             
                <label>
                <div className="title_label ">Obj. Sistem origen:</div>
                  <select type="text" name="name" className="input_half" disabled>
                    <option>Select an option</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Don't wish to identify</option>
                  </select>
                  <input type="text" name="name" className="input_half" disabled/>
                 
                </label>
              </div>
              <div className="input_icon">
                
            
                <label>
                <div className="title_label ">Tipolog. Contable:</div>
                  <select
                    type="text"
                    name="name"
                    className="input_normal"
                    value={tipologiaSelected}
                    onChange={(e) => setTipologiaSelected(e.target.value)}
                  >
                    {tipologiaList.map((tipologiaContable, index) => (
                      <option>{tipologiaContable}</option>
                    ))}
                  </select>
                </label>
                
                <div className="icon">
                  <MdCleaningServices onClick={handleCleanTipologia} />
                  <FcInfo />
                </div>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label "> F. Asiento:</div>
                  <input type="date" className="input_half" disabled /> 
                  <input type="date" className="input_half" disabled />
                </label>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label "> Importe:</div> 
                  <input type="text" name="name" className="input_half" disabled/> 
                  <input type="text" name="name" className="input_half" disabled/>
                </label>
              </div>
              <div className="input_icon">
                <label>
                <div className="title_label "> Divisa:</div>  
                  <select
                    type="text"
                    name="name"
                    className="input_normal"
                    value={divisaSelected}
                    onChange={(e) => setDivisaSelected(e.target.value)}
                    disabled>
                    <option>Select an option</option>
                    <option value="Opt1">Option1</option>
                    <option value="Opt2">Option2</option>
                    <option value="Opt3">Option3</option>
                  </select>
                </label>
                <div className="icon">
                  <MdCleaningServices onClick={handleCleanDivisa} />
                  <FcInfo />
                </div>
              </div>
            </Col>
          </Row>
        </div>

        <div className="buttons">
        <div className={ (disabledSearch ? "disabled" : "button")} onClick={(disabledSearch ? ()=>{} : handleFilter)}>
            <FcSearch />
            Buscar
          </div>
          <div className="button" onClick={handleCleanAll}>
            <MdCleaningServices />
            Limpiar
          </div>
        </div>
      </form>
    </div>
  );
}

export default Form;
