import React, { useState, useCallback } from "react";
import Form from "../Form/Form";
import Table from "../Table/Table";
import "./HomePage.css";
import { HiMinus, HiPlus } from "react-icons/hi";
import data from "../../data.json";

function Home() {
    const [show, setShow] = useState(false);
    const [hidden, setHidden] = useState(false);

    const [officesFiltered, setOfficesFiltered] = useState(data.offices);

    const callback = useCallback((newFilteredOffices) => {
        setOfficesFiltered(newFilteredOffices);
    }, []);
    
    return (
        <div>
            <div className="div_home">
                <div>
                    <button className="buttonshow busqueda" onClick={() => setShow(!show)}>{show ? <HiMinus className="minus" /> : <HiPlus className="minus"/>} Criterios de Búsqueda</button>
                    <div>
                        {show && <Form parentCallback={callback} officeList={data.offices} />}
                    </div>
                </div>
            </div>

            <div className="div_home">
                <div>

                    <button className="buttonshow busqueda" onClick={() => setHidden(!hidden)}>{hidden ? <HiMinus className="minus" /> : <HiPlus className="minus"/>} Resultados de la Búsqueda </button>
                    <div>
                        {hidden && <Table officeList={officesFiltered} />}
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Home;
