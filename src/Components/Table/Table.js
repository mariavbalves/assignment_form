import React, { useState, useEffect } from "react";
import "./Table.css";
import { RiFileExcel2Fill } from "react-icons/ri";



function Table(props) {
  const [checkList, setCheckList] = useState([]);

  useEffect(() => {
    var newList=[];
    props.officeList.forEach(() => {
      newList.push(false);
    });
    setCheckList(newList);
  },[]);

  const Checkbox = (index) => { 
    var newCheckList = checkList;
    newCheckList[index] = !newCheckList[index];
    setCheckList(newCheckList);
  }
  const handlePrintOffices = () => {
    checkList.forEach((currentCheckbox, index) => {
      if (currentCheckbox) {
        console.log(props.officeList[index]);
      }
    });
  };
  // const toggleCheckList = (bool) => {
  //   var newCheckList = checkList;
  //   newCheckList.forEach((index) =>{
  //     newCheckList[index] = bool;
  //   });
  //   console.log("Toggle: " + newCheckList);
  //   setCheckList(newCheckList);
  // }
  const handleAllCheckboxes = () =>{
    // var toggled = false;
    // var firstCheckbox = checkList[0];

    // checkList.forEach((currentCheckbox)=>{
    //     if((currentCheckbox !== firstCheckbox) && !toggled){
    //       toggleCheckList(true);
    //       firstCheckbox = true;
    //       toggled = true;
    //     }
    // });

    // if(!toggled){
    //     console.log("It was me " + !firstCheckbox);
    //     toggleCheckList(!firstCheckbox)
    // }

    // console.log("ToggleAll:" +checkList);
  }

  return (
    <div>
    <div className="tableContainer">
       <div className="excelicon"><RiFileExcel2Fill/></div>
       <div className="results"><p>Mostrando {props.officeList.length} de {props.officeList.length} resultados</p></div>
      <div className="table_wrapper">
      <table className="table">
        <thead>
          <tr>
            <th className="tcell" scope="col"> #
            </th>
            <th className="tcell" scope="col"></th>
            <th className="tcell" scope="col">Id Prot.
            </th>
            <th className="tcell" scope="col">Tipologia Contable
            </th>
            <th className="tcell" scope="col">Instalación
            </th>
            <th className="tcell" scope="col">Empresa
            </th>
            <th className="tcell" scope="col">F. Ini.
            </th>
            <th className="tcell" scope="col">Estado
            </th>
             <th className="tcell" scope="col">Sistema
            </th>
          </tr>
        </thead>
        <tbody className="list">
          {props.officeList.map((office, index) =>(
            <tr>
              <th className="tcell" scope="row">{index}</th>
              <td className="tcell">
                  <input type="checkbox" value={checkList[index]} onChange={(()=>(Checkbox(index)))}></input>
                </td>
              <td className="tcell">{office.idProt}</td>
              <td className="tcell">{office.tipologiaContable}</td>
              <td className="tcell">{office.instalacion}</td>
              <td className="tcell">{office.empresa}</td>
              <td className="tcell">{office.fini}</td>
              <td className="tcell">{office.estado}</td>
              <td className="tcell">{office.sistema}</td>
            </tr>
          ))}
        
        </tbody>
      </table>
      </div>

      <div className="buttonstable">
          <button className="button" onClick={handlePrintOffices}>
           Mostrar Selecionados
          </button>
          <button className="button" onClick={handleAllCheckboxes}>
            Des/Seleccionar todo
          </button>
        </div>

    </div>
    <div className="buttons">
          <div className="textspaced">Selecione: </div>
          <button className="button">
          <select>
          <option>Selecione una opcion</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
          </select>
          </button>

          <button className="button">
           Acceptar
          </button>
        </div>
    </div>);
}

export default Table;
